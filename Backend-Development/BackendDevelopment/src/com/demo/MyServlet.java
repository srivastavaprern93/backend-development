package com.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/myServlet")
public class MyServlet extends HttpServlet {

	public void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		PrintWriter pw = resp.getWriter();
		pw.println("Hii ");

//		ServletContext con = getServletContext();
//		String country =  con.getInitParameter("country");
//		pw.println(country);

		ServletConfig conf = getServletConfig();
		String country = conf.getInitParameter("country");
		pw.println(country);
	}

}
