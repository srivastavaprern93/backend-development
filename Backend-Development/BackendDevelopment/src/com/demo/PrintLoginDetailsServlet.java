package com.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PrintLoginDetailsServlet extends HttpServlet {

//	public void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		String name = req.getParameter("userName");
//		String password = req.getParameter("password");
//		PrintWriter pw = resp.getWriter();
//		pw.println("Hello "+name);
//		pw.println("Youe password is " + password);
//	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String name = req.getParameter("userName");
		String password = req.getParameter("password");
		PrintWriter pw = resp.getWriter();
		pw.println("Hello " + name);
		pw.println("Your password is " + password);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String name = req.getParameter("userName");
		String password = req.getParameter("password");
		PrintWriter pw = resp.getWriter();

//		req.setAttribute("nm", name);
//		req.setAttribute("pass", password);
//		
//		RequestDispatcher rd = req.getRequestDispatcher("message");// url of servlet to be called
//		rd.forward(req, resp);

//		pw.println("Hello " + name);
//		pw.println("Youe password is " + password);

//		resp.sendRedirect("message?nm="+name);//URL Rewriting

//		HttpSession session = req.getSession();
//		session.setAttribute("name", name);
//		session.setAttribute("pass", password);
//		resp.sendRedirect("message");

		Cookie cookie = new Cookie("name", name);
		Cookie cookie1 = new Cookie("pass", password);

		resp.addCookie(cookie);
		resp.addCookie(cookie1);
		
		resp.sendRedirect("message");
	}

}
