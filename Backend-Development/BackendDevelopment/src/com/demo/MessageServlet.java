package com.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MessageServlet extends HttpServlet {

	public void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {

//		String userName = (String)req.getAttribute("nm");
//		String password = (String)req.getAttribute("pass");

//		String userName = (String)req.getParameter("nm");
		PrintWriter pw = resp.getWriter();

//		HttpSession session = req.getSession();
//		String userName = (String) session.getAttribute("name");
//		String password = (String) session.getAttribute("pass");
//
//		pw.println("Second Servlet!!");
//		pw.println("Name is: " + userName);
//		pw.println("Password is: " + password);

		Cookie[] cookies = req.getCookies();
		String userName="", password="";
		for(Cookie c : cookies) {
			if(c.getName().equals("name")) {
				userName = (String)c.getValue();
			}
			
			if(c.getName().equals("pass")) {
				password = (String)c.getValue();
			}
		}
		
		pw.println("Second Servlet!!");
		pw.println("Name is: " + userName);
		pw.println("Password is: " + password);
	}

}
