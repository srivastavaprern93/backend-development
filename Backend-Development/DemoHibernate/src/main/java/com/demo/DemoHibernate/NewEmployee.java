package com.demo.DemoHibernate;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class NewEmployee {

	@Id
	private int employeeId;
	private String name;
	private String contact;

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

}
